#%%
import os
import numpy as np
import scipy.sparse
import scipy.sparse.linalg
import time

#%%
class BlockCoordinateDescentSolver:

    def __init__(self, particles, springs, activeNodes, activeNodes2D, posFixedNodes2D, gravity, timestep, energy):
        self.particles = particles
        self.springs = springs
        self.timestep = timestep
        self.alpha = 0 #1 - 0.05 * timestep

        self.energy = energy

        self.np = self.particles.shape[0]
        self.ns = self.springs.shape[0]
        self.ndim = self.particles['x'].shape[1]

        self.M = self.compute_M()
        self.L = self.compute_L()
        self.J = self.compute_J()

        self.d = np.empty((self.ns * self.ndim, 1))
        self.g = np.kron(activeNodes, gravity)

        self.b = np.zeros((self.np * self.ndim, 1))
        self.bfix = np.zeros((self.np * self.ndim, 1)) 

        # Prefactor M + L * timestep**2
        A = self.M + self.L * self.timestep**2    
        self.solveAxb = scipy.sparse.linalg.factorized(A)

        #añadir la prefectorización de u y e

    def compute_A(self, i):
        """Returns the incidence vector for the i-th spring of size m x 1"""
        i1 = self.springs[i]['first']
        i2 = self.springs[i]['second']
        return scipy.sparse.csc_matrix(([1., -1.], ([i1, i2], [0, 0])), shape=(self.np, 1))

    def compute_S(self, i):
        """Returns the i-th spring indicator of size s x 1"""
        return scipy.sparse.csc_matrix(([1], ([i], [0])), shape=(self.ns, 1))

    def compute_L(self):
        """Computes the stiffness-weighted Laplacian of the mass-spring graph. L is m*ndims x m*ndims """
        l = scipy.sparse.csc_matrix((self.np, self.np))

        for i in range(self.springs.shape[0]):
            A = self.compute_A(i)
            l += self.springs[i]['k'] * A * A.transpose()

        return scipy.sparse.kron(l, np.eye(self.ndim, self.ndim), format='csc')

    def compute_M(self):
        """Computes the diagonal mass matrix. M is m*ndims x m*ndims """
        return scipy.sparse.kron(scipy.sparse.diags(self.particles['m']), np.eye(self.ndim, self.ndim), format='csc')

    def compute_J(self):
        """Computes J. J is m*ndims x s*ndims """
        j = scipy.sparse.csc_matrix((self.np, self.ns))

        for i in range(self.ns):
            s = self.springs[i]
            A = self.compute_A(i)
            j += s['k'] * A * self.compute_S(i).transpose()

        return scipy.sparse.kron(j, np.eye(self.ndim, self.ndim), format='csc')
    
    def update_b(self):
        # inertia
        self.bfix = self.M * ((1 + self.alpha) * self.particles['x'] - self.alpha * self.particles['xprev']).reshape(-1, 1)
        # external forces
        self.bfix += self.M * self.g * self.timestep**2

        self.particles['xprev'][:] = self.particles['x']

    def update_d(self):
        """Updates the spring lengths to optimal rest length values while keeping their directions."""

        def normalized(a):
            """Normalizes each row of a."""
            n = np.apply_along_axis(np.linalg.norm, 1, a)
            n[n==0.] = 1 # Leave as is for zero vectors
            return a / n.reshape(-1, 1)

        # Compute the directions of springs using the current position of the particles
        self.d[:] = (np.matmul(np.diag(springs['r']), normalized(particles['x'][springs['first']] - particles['x'][springs['second']]))).reshape(-1, 1) 

    def update_x(self):
        """Computes new particles positions by solving Ax=b."""
        # Update the right hand side
        self.b = self.bfix + self.J * self.d * self.timestep**2

        #cambiar la prefactorización en el punto -> 

        # Solve global step
        auxParticles = self.solveAxb(self.b).reshape(self.np, -1)
        #se resetean las posiciones de los nodos fijos
        auxParticles = auxParticles * activeNodes2D + posFixedNodes2D
        self.particles['x'][:] = auxParticles

    def update_energy(self):
        particles['kineticEnergy'] = 0.0
        springs['potentialEnergy'] = 0.0

        #Kinetic energy 1/2*m*v^2
        particles['vel'] = 1/timeStep * (particles['xprev']-particles['x'])
        velnorm = np.apply_along_axis(np.linalg.norm, 1, particles['vel']) **2
        particles['kineticEnergy'] = 0.5 * particles['m'] * velnorm
        self.energy[0] = particles['kineticEnergy'].sum()

        #potential energy 1/2*k*1/r*(xnorm - r)^2
        xnorm = np.apply_along_axis(np.linalg.norm, 1, particles['x'][springs['first']] - particles['x'][springs['second']])
        springs['potentialEnergy'] = 0.5 * springs['k'] * (1.0/springs['r']) * (xnorm - springs['r'])**2
        self.energy[1] = (springs['potentialEnergy']).sum()

        #potential energy mgh
        particles['potentialEnergy'] = particles['m'] * Gravity[1][0] * particles['x'][:,1]
        self.energy[2] += (particles['potentialEnergy']).sum()

    def solve(self, iterations):
        self.update_b()
        for _ in range(iterations):
            self.update_d()
            self.update_x()
        self.update_energy()



def writeDataDynamic():

    sw_dynamic = open(pathWriteTextDynamic + codeTest + "_" + (str("{0:.5f}".format(currentTime))).ljust(9, '0') + ".pos", "w")
    sw_dynamic.write("0" + "\n")
    sw_dynamic.write("1" + "\n")
    sw_dynamic.write(str(NumDof) + "\n")
    for i in range(0, NumDof):
        sw_dynamic.write(str(particles[i]['x'][0]) + "\n")
        sw_dynamic.write(str(particles[i]['x'][1]) + "\n")
        sw_dynamic.write(str(0.0) + "\n")

    sw_dynamic.close()

def writeDataTestEnergy():
    global prevEnergy
    global slope
    global kineticEnergyPeriod
    global countKineticEnergyPeriod
    global sumKineticEnergyPeriod
    global timePrevPeak
    global peak
    global totalPeakKinetic
    global goingUp
    global stopSimulation

    slope = (energy[0] - prevEnergy) / timeStep
    if slope < 0.0 and goingUp:
        kineticEnergyPeriod = currentTime - timePrevPeak
        countKineticEnergyPeriod += 1
        sumKineticEnergyPeriod += kineticEnergyPeriod
        timePrevPeak = currentTime
        peak = prevEnergy
        
        totalPeakKinetic += 1
        goingUp = False

        sw_dataTestEnergyPeriod.write(str(currentTime) + ";" + str(numIt) + ";" + str(energy[0]) + ";" + str(kineticEnergyPeriod) + "\n")
        
        if peak < thresholdEnergy:
            stopSimulation = True
    else:
        if (slope > 0.0) and (not goingUp):
            goingUp = True
    prevEnergy = energy[0]

    sw_dataTestEnergy.write(str(currentTime) + ";" + str(energy[0]) + ";" + str(energy[1]) + ";" + str(energy[2]) + "\n")

def writeDataTest():

    sw_dataTest = open(pathWriteTextTest + codeTest + ".dtest", "w")

    TkineticEnAvg = sumKineticEnergyPeriod / countKineticEnergyPeriod

    #TODO method error
    sw_dataTest.write("idSolver;dt;stiff;iter/step;totalSolverIter;error;totalPeakKinetic;Tavg" + "\n")
    sw_dataTest.write(str(typeSolver) + ";" + str(timeStep) + ";" + str(Stiffness) + ";" + str(iterationsSolver) + ";" + str(numIt) + ";" + str(0) + ";" + str(totalPeakKinetic) + ";" + str(TkineticEnAvg) + "\n")

    sw_dataTest.write("nodes;springs;mass" + "\n")
    sw_dataTest.write(str(NumDof) +  ";" + str(NumDof-1) + ";" + str(Mass) + "\n")

    sw_dataTest.write("FinalPos" + "\n")
    for i in range(0, NumDof):
        sw_dataTest.write(str(particles[i]['x'][0]) + "\n")
        sw_dataTest.write(str(particles[i]['x'][1]) + "\n")
        sw_dataTest.write(str(0.0) + "\n")

    sw_dataTest.close()

def writeDataPosProcessing():
    for i in range(0, NumDof):
        #posX;posY;posZ;id
        sw_dataProcessing.write(str(particles[i]['x'][0]) + ";" + str(particles[i]['x'][1]) + ";" + str(0.0) + ";" + str(i) + "\n")

#%%
#GENERAL VARIABLES
Dim = 2
DomainSize = 1
Gravity = np.zeros((Dim, 1))
Gravity[1][0] = -9.8
typeSolver = 2 #0: newton, 1: xpbd, 2: liu13, 3: admm, 4: pbd

codeTest = "CatFix10_2-01-1e6-30_ja" #code-solver-dt-stiff-it
NumDof = 10
Stiffness = 1000000
timeStep = 0.01
iterationsSolver = 30
Mass = 1.

#ENERGY VARIABLES
goingUp = True
stopSimulation = False
thresholdEnergy = 0.0001
slope = 0.0
peak = 0.0 
timePrevPeak = 0.0
prevEnergy = 0.0
kineticEnergyPeriod = 0.0
countKineticEnergyPeriod = 0
sumKineticEnergyPeriod = 0.0
totalPeakKinetic = 0
energy = [0.0, 0.0, 0.0] #kinetic, spring potential, grav potential

numIt = 0
currentTime = 0.0
totalTimeExec = 0.0

#INITIALIZE
particles = np.zeros(NumDof, dtype=[
    ('x', float, Dim),
    ('xprev', float, Dim),
    ('vel', float, Dim),
    ('m', float),
    ('kineticEnergy', float),
    ('potentialEnergy', float),
])

springs = np.zeros(NumDof - 1, dtype=[
    ('first', int),
    ('second', int),
    ('k', float),
    ('r', float),
    ('potentialEnergy', float)
])

particles['m'] = 1.

#todos los nodos en fila separados 1 unidad
for i in range(0, NumDof):
    particles[i]['x'] = [i, 0.0]
particles['xprev'][:] = particles['x']

springs['k'] = Stiffness
springs['r'] = 1.

for i in range(0, NumDof-1):
    springs[i]['first'] = i
    springs[i]['second'] = i+1

# fix boundary nodes TODO: update new version of boundary condition
particles[0]['m'] = 1000000.
particles[NumDof-1]['m'] = 1000000.
activeNodes = np.ones((NumDof, 1))
activeNodes[0] = 0
activeNodes[NumDof-1] = 0

#activeNodes repeating each node along the diagonal of the matrix
#activeNodes2D = scipy.sparse.kron(scipy.sparse.diags(activeNodes), np.eye(particles['x'].shape[1], particles['x'].shape[1]), format='csc')
activeNodes2D = np.ones((NumDof,2))
activeNodes2D[0] = 0
activeNodes2D[NumDof-1] = 0

#activeNodes2DM = scipy.sparse.kron(scipy.sparse.diags(activeNodes2D), np.eye(particles['x'].shape[1], particles['x'].shape[1]), format='csc')
    
posFixedNodes2D = np.zeros((NumDof, 2))
posFixedNodes2D[0] = particles[0]['x']
posFixedNodes2D[NumDof-1] = particles[NumDof-1]['x']


#DATA TEST VARIABLES

#folders path
projectDir = os.path.abspath(os.path.dirname(__file__))
appPathDTestVisualProc = projectDir + "\\DataTests\\" + codeTest
appPathDTestVisualBlend = projectDir + "\\DataTests\\" + codeTest + "\\" + codeTest + "_visual"
appPathDTest = projectDir + "\\DataTests\\" + codeTest + "\\" + codeTest + "_data"

os.makedirs(appPathDTestVisualProc, exist_ok=True)
os.makedirs(appPathDTestVisualBlend, exist_ok=True)
os.makedirs(appPathDTest, exist_ok=True)

pathWriteTextTest = appPathDTest + "\\dataTest_"
pathWriteTextTestEnergy = appPathDTest + "\\dataTestEnergy_"
pathWriteTextTestEnPeriod = appPathDTest + "\\dataTestEnergyPeriod_"
pathWriteTextProcessing = appPathDTestVisualProc + "\\posProcessing_"
pathWriteTextTopology = appPathDTestVisualBlend + "\\topology_"
pathWriteTextDynamic = appPathDTestVisualBlend + "\\positions_"

#TODO
#writeDataTopology()
writeDataDynamic()

sw_dataTestEnergy = open(pathWriteTextTestEnergy + codeTest + ".dtest", "w")
sw_dataTestEnergy.write("time;kinetic;potential;gravPotential" + "\n")

sw_dataTestEnergyPeriod = open(pathWriteTextTestEnPeriod + codeTest + ".dtest", "w")
sw_dataTestEnergyPeriod.write("time;solverIter;kinetic;T" + "\n")

sw_dataProcessing = open(pathWriteTextProcessing + codeTest + ".dproc", "w")
#1st line: typeSolver(2), deltaTime, iterations, numNodes, numSprings, mass/particle, stiffness
sw_dataProcessing.write(str(typeSolver) + ";" + str(timeStep) + ";" + str(iterationsSolver) + ";" + str(NumDof) + ";" + str((NumDof-1)) + ";" + str(Mass) + ";" +  str(Stiffness) + "\n")


#%%


#%%
solver = BlockCoordinateDescentSolver(particles, springs, activeNodes, activeNodes2D, posFixedNodes2D, Gravity, timeStep, energy)


#%%
import sys
from OpenGL.GL import *
from OpenGL.GL import shaders
from OpenGL.GLU import *
from OpenGL.GLUT import *
from IPython.core.debugger import set_trace

vertex_shader = """
#version 330
in vec3 position;
void main()
{
   gl_Position = vec4(position, 1.f);
}
"""

fragment_shader = """
#version 330
void main()
{
   gl_FragColor = vec4(0.5f, 1.0f, 1.0f, 1.0f);
}
"""

def create_vbo(shader):
    vertex_array_object = glGenVertexArrays(1)
    glBindVertexArray(vertex_array_object)
    
    vertex_buffer = glGenBuffers(1)
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer)
    
    position = glGetAttribLocation(shader, 'position')
    glEnableVertexAttribArray(position)
    
    glVertexAttribPointer(position, 2, GL_DOUBLE, False, 0, ctypes.c_void_p(0))
    
    glBufferData(GL_ARRAY_BUFFER, particles['x'].nbytes, particles['x'], GL_DYNAMIC_DRAW)
    
    glBindVertexArray(0)

    glDisableVertexAttribArray(position)
    glBindBuffer(GL_ARRAY_BUFFER, 0)
    
    return vertex_array_object, vertex_buffer



def display(w, h):
    if not stopSimulation:

        IniTime = time.time() 
        solver.solve(iterationsSolver)
        FnalTime = time.time()  
        executionTime = FnalTime - IniTime
        global totalTimeExec
        totalTimeExec += executionTime
        global currentTime 
        currentTime += timeStep
        global numIt
        numIt = numIt + 1

        writeDataDynamic()
        writeDataTestEnergy()
        writeDataPosProcessing()

        if stopSimulation:
            writeDataTest()
            sw_dataTestEnergy.close()
            sw_dataTestEnergyPeriod.close()
            sw_dataProcessing.close()
            print("STOP SIMULATION: ", currentTime, "s", end='\n')
            print("TIME: ", totalTimeExec, "s", end='\n')

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glUseProgram(shader)
    
    #gluLookAt (0.0, 0.0, 5.0, 100.0, 0.0, 0.0, 0.0, 1.0, 0.0)

    glBindVertexArray(vertex_array_object)

    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer)
    glBufferData(GL_ARRAY_BUFFER, particles['x'].nbytes, particles['x'], GL_DYNAMIC_DRAW)
    glBindBuffer(GL_ARRAY_BUFFER, 0)

    glDrawArrays(GL_LINE_STRIP, 0, particles.shape[0])
    glBindVertexArray(0)
    
    glUseProgram(0)

    glutSwapBuffers()

def reshape(w, h):
    glViewport(0, 0, w, h)
    #glMatrixMode (GL_PROJECTION)

    glutDisplayFunc(lambda: display(w, h))
    glutPostRedisplay()

glutInit(sys.argv)
glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH)
glutInitWindowSize(400,400)
glutCreateWindow(b'2013 SigAsia Liu')
glutReshapeFunc(reshape)
glutIdleFunc(glutPostRedisplay)

glClearColor(0.5, 0.5, 0.5, 1.0)
glEnable(GL_DEPTH_TEST)
shader = OpenGL.GL.shaders.compileProgram(
    shaders.compileShader(vertex_shader, GL_VERTEX_SHADER),
    shaders.compileShader(fragment_shader, GL_FRAGMENT_SHADER))

vertex_array_object, vertex_buffer = create_vbo(shader)

glutMainLoop()


#%%



#%%



#%%



#%%



